import React from 'react'
import MealsSummary from "./MealsSummery.js";
import AvailableMeals from "./AvailableMeals.js"

export default function Meals(props) {
    return (
        <>
        <MealsSummary/>
        <AvailableMeals/>
        </>
    )
}