import React from 'react'
import mealsImage from "../../../assets/FoodOrder/meals.jpg"
import classes from './HeaderCardButton.module.css'

export default function Header(props) {
    return (
        <>
            <haeder className={classes.header}>
                <h1>React Meals</h1>

            </haeder>
            <div className={classes['main-image']}>
                <img src={mealsImage}/>
            </div>
        </>
    )
}