import { useContext, useEffect, useState } from 'react';
import CartIcon from '../Cart/CartIcon.js';
import classes from './HeaderCardButton.module.css'
import CartContext from '../../../store/FoodOrder/cart-context.js';

export default function HeaderCardButton(props) {
    const [btnIsHighlited, setBtnIsHighlited] = useState(false);
    const cartCtx = useContext(CartContext);

    const { items } = cartCtx;

    const numberOfCartItems = items.reduce((carNumber, item) => {
        return carNumber + item.amount
    },0)


    const btnClasses = `${classes.button} ${btnIsHighlited ? classes.bump : ''}`;

    useEffect(() => {
        if(items.length === 0) {
            return;
        }

        setBtnIsHighlited(true);

        const timer = setTimeout(() => {
            setBtnIsHighlited(false)
        },300);

        return () => {
            clearTimeout(timer)
        }
    }, [items])

    return (
        <button className={btnClasses} onClick={props.onClick}>
            <span className={classes.icon}>
                <CartIcon />
            </span>
            <span>Your Cart</span>
            <span className={classes.badge}>{numberOfCartItems}</span>
        </button>
    )

}