import React from 'react'
import ExpenseForm from "./ExpenseForm.js"
import './NewExpense.css'

export default function NewExpense() {
    return (
        <div className='new-expense'>
            <ExpenseForm/>
        </div>
    )
}