import React, { useState } from 'react';

import Card from '../UI/Card.js';
import ExpenseItem from "./ExpenseItem.js"

import './Expense.css';

export default function Expense(props) {
    const loops = () => {
        props.items?.map((item) => {
            console.log(item.id)
        })
    }

    
    return (
        <div>
            <Card className="expenses">
                {
                    props.items?.map((item) => {
                        return (
                            <ExpenseItem date={item.date} title={item.title} amount={item.amount} id={item.id} />
                        )
                    })
                }
            </Card>
        </div>
    )
}