import React, { useState } from 'react';

import ExpenseDate from './ExpenseDate.js';
import Card from '../UI/Card.js';
import './ExpenseItem.css';

export default function ExpenseItem(props) {
    const [item, setItem] = useState(props.title);
    const clickHandler = () => {
        setItem('Update');
    }

    return (
    <Card className='expense-item'>
      <ExpenseDate date={props.date} />
      <div className='expense-item__description'>
        <h2>{item ? item : props.title}</h2>
        <div className='expense-item__price'>${props.amount}</div>
      </div>
      <button onClick={clickHandler}>Change Title</button>
    </Card>
    )
}