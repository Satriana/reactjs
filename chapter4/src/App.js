import React,{ useState, useContext } from 'react';

import CartProvider from './store/FoodOrder/CardProvider.js';
import Cart from './components/FoodOrder/Cart/Cart.js';
import Meals from "./components/FoodOrder/Meals/Meals.js"
import Header from './components/FoodOrder/Layout/Header.js';


function App() {
  // const expenses = [
  //   {
  //     id: 'e1',
  //     title: 'Toilet Paper',
  //     amount: 94.12,
  //     date: new Date(2020, 7, 14),
  //   },
  //   { id: 'e2', title: 'New TV', amount: 799.49, date: new Date(2021, 2, 12) },
  //   {
  //     id: 'e3',
  //     title: 'Car Insurance',
  //     amount: 294.67,
  //     date: new Date(2021, 2, 28),
  //   },
  //   {
  //     id: 'e4',
  //     title: 'New Desk (Wooden)',
  //     amount: 450,
  //     date: new Date(2021, 5, 12),
  //   },
  // ];

  // DynamicInline Bab 6
  // const [courseGoals, setCourseGoals] = useState([
  //   { text: 'Do all exercises!', id: 'g1' },
  //   { text: 'Finish the course!', id: 'g2' }
  // ])

  // const addGoalHandler = enteredText => {
  //   setCourseGoals(prevGoals => {
  //     const updatedGoal = [...prevGoals];
  //     updatedGoal.unshift({text: enteredText, id: Math.random().toString()});
  //     return updatedGoal;
  //   })
  // }

  // const deleteItemHandler = goalId => {
  //   setCourseGoals(prevGoals => {
  //     const updatedGoals = prevGoals.filter(goal => goal.id !== goalId);
  //     console.log(updatedGoals);
  //     return updatedGoals;
  //   })
  // }

  // let content = (
  //   <p style={{ textAlign: 'center' }}>No goals found. Maybe add one?</p>
  // );

  // if(courseGoals.length > 0) {
  //   content = (<CourseGoalList items={courseGoals} onDeleteItem={deleteItemHandler} />)
  // }

  // BAB 8
  // const [usersList, setUsersList] = useState([]);

  // const addUserHandler = (uName, uAge) => {
  //   setUsersList((prevUsers) => {
  //     return [
  //       ...prevUsers,
  //       {name: uName, age: uAge, id: Math.random().toString()}
  //     ];
  //   });
  // }

  // Bab 10.
  // const ctx = useContext(AuthContext);

  // Bab 11.
  const [cartIsShown, setCartIsShown] = useState(false);

  const showCartHandler = () => {
    setCartIsShown(true)
  }

  const hideCartHandler = () => {
    setCartIsShown(false)
  }


  return (
    // <div className="App">
    //   Start from 03
    //     <NewExpense/>
    //   End 
    // </div>
    
    <React.Fragment>
      {/*
      BAB 6
      <section id='goal-form'>
        <CourseInput onAddGoal={addGoalHandler}/>
      </section>
      <section id='goals'>
        {content}
      </section> 
      */}

      {/*
      Bab 8 dan 9 (createPortal)
      <AddUser onAddUser={addUserHandler}/>
      <UserList users={usersList}/>
      {
        ReactDOM.createPortal(
          <p>This child is placed in the document body.</p>,
          document.body
        )
      } */}

      {/* Bab 10
      <MainHeader>
      <main>
        {!ctx.isLoggedIn && <Login />}
        {ctx.isLoggedIn && <Home />}
      </main>
      </MainHeader> */}

      <CartProvider>
      {cartIsShown && <Cart onClose={hideCartHandler} />}
      <Header onShowCart={showCartHandler} />
      <main>
        <Meals />
      </main>
    </CartProvider>
    </React.Fragment>

  );
}

export default App;
