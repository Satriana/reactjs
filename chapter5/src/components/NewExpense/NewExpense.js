import React from 'react';
import ExpenseForm from './ExpenseForm.js';
import './NewExpense.css';

export default function newExpense(props) {
    const saveExpenseDataHandler = (enteredExpenseData) => {
        const expenseData = {
            ...enteredExpenseData,
            id: Math.random().toString
        };
        props.onAddExpense(expenseData);
    };



    return(
    <div className='new-expense'>
        <ExpenseForm onSaveExpenseData={saveExpenseDataHandler} />
    </div>
    )
}