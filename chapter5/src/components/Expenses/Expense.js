import React, { useState } from 'react';

import Card from '../UI/Card.js';
import ExpensesFilter from './ExpenseFilter.js';
import ExpensesList from './ExpenseList.js';
import ExpensesChart from './ExpenseChart.js';
import './Expense.css';

export default function Expense(props) {
    const [filteredYear, setFilteredYear] = useState('2020');

  const filterChangeHandler = (selectedYear) => {
    setFilteredYear(selectedYear);
  };

  const filteredExpenses = props.items.filter((expense) => {
    return expense.date.getFullYear().toString() === filteredYear;
  });

  return (
    <div>
      <Card className='expenses'>
        <ExpensesFilter
          selected={filteredYear}
          onChangeFilter={filterChangeHandler}
        />
        <ExpensesChart expenses={filteredExpenses} />
        <ExpensesList items={filteredExpenses} />
      </Card>
    </div>
  );
}